$(document).ready(function() {
  var $window = $(window),
      $body   = $('body'),
      $wrap   = $('.wrap'),
      $header = $('header');

  //Body Lock Scroll

  $('#mobile_menu').click(function () {
    $body.toggleClass('lock_position');
  });

  //Mobile Menu

  $('#mobile_menu').click(function () {
    $('.side_bar-menu').toggleClass('open');
  });

  //Footer > links add active and visited class

  var thisHref            = location.href,
      chapterActive       = chapterNumb(thisHref),
      $headerNavItem      = $('.side_bar-menu .nav_item'),
      $headerStoryNavItem = $('.side_bar-menu .nav-story_item'),
      $progress           = $('footer .progress'),
      $footerNavItem      = $('footer .nav_item'),
      progressW           = $($footerNavItem[$footerNavItem.length - 1]).offset().left - $($footerNavItem[0]).offset().left;

  function addActiveClass() {
    if (chapterActive === 0) {
      $($headerNavItem[chapterActive]).addClass('active');
      $($footerNavItem[chapterActive]).addClass('active');
    } else if (chapterActive > 0) {
      $($headerStoryNavItem[chapterActive - 1]).addClass('active');

      $footerNavItem.each(function(index) {
        if (index < chapterActive) {
          $(this).addClass('visited');
        }
      });

      $($footerNavItem[chapterActive]).addClass('active');

      $progress.css({
        'width' : progressW/($footerNavItem.length - 1) * chapterActive,
        'z-index' : 1
      });
    } else if (chapterActive === 'about-me') {
      $($headerNavItem[$headerNavItem.length - 1]).addClass('active');
    }
  }

  addActiveClass();

  $window.resize(function() {
    progressW = $($footerNavItem[$footerNavItem.length - 1]).offset().left - $($footerNavItem[0]).offset().left;
    addActiveClass();
  });

  function chapterNumb(e) {
    var arr = e.split(/[/.]/);
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].indexOf('chapter') != '-1') {
        return arr[i].replace('chapter-', '');
      } else if (arr[i].indexOf('about-me') != '-1') {
        return 'about-me';
      }
    }
    return 0;
  }

  //Resize Video

  function resizeVideo(param) {
    var $videoParent = $(param.videoParent),
        $video = $(param.videoParent + ' ' + param.video),
        videoH = $videoParent.outerHeight() + 30,
        videoW = videoH * 2.1;

    function setSize() {
      $video.css({
        'min-width' : videoW
      });
    }
    setSize();
    $window.resize(function () {
      videoH = $videoParent.outerHeight();
      videoW = videoH * 2.1;
      setSize();
    });
  }


  //Function Run ----------------------------------------

  if ($('body').hasClass('index')) {
    resizeVideo({
      videoParent: '.bg_video',
      video:       'video'
    });
  } else if ($('body').hasClass('nav_page')) {
    var $singleSlider = $('.slider-wrap.single .slider'),
        $multiSlider  = $('.slider-wrap.multi .slider');

      $singleSlider.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      swipeToSlide: true,
      touchThreshold: 30
    });

    $multiSlider.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      swipeToSlide: true,
      touchThreshold: 30,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
  }
});

var $buoop = {
  vs: {i:11,f:-4,o:-2,s:9,c:-6},
  reminder: 0,
  api:4
};
function $buo_f(){
  var e = document.createElement("script");
  e.src = "//browser-update.org/update.min.js";
  document.body.appendChild(e);
}
try {
  document.addEventListener("DOMContentLoaded", $buo_f, false);
}
catch(e){
  window.attachEvent("onload", $buo_f);
}
