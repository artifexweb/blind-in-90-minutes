<?php
/**
 * dee.ie functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package 90min
 */
// Define the version so we can easily replace it throughout the theme
define( 'THE_VERSION', 0.01 );
define( 'THE_THEME', get_stylesheet_directory_uri() );


function theme_setup() {

	 /**
	 * Support title tag and wordpage can generate title
	 */
	add_theme_support( 'title-tag' );

	 /**
	 * Custom image sizes for media upload and use
	 */
	include_once('inc/custom_image_sizes.php');

	/*
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'main_menu' => esc_html__( 'Primary', 'prime-nav' ),
	) );

	/**
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Enable support for all Post Formats.
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/**
	 * Add theme support for selective refresh for widgets.
	 */
	add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'theme_setup' );


/**
 * Enqueues scripts and styles
 */
include_once('inc/enqueue_scripts.php');


/**
 * Load mandatory theme plugins
 */
include_once('inc/required_plugins.php');



/**
 * include: custom admin styles
 */
function load_admin_styles() {
	wp_enqueue_style( 'admin_css_dee', get_template_directory_uri() . '/css/admin-style.css', false, THE_VERSION );
}
add_action( 'admin_enqueue_scripts', 'load_admin_styles' );


/**
 * Defer JavaScript load (simple way)
 */
function defer_parsing_of_js ( $url ) {

	// This should match every possible way to load jQuery
	$re = '/\b(jquery-\d.\d{1,2}.\d{1,2}.min.js)|(jquery[.]js)/';
	preg_match_all($re, $url, $matches, PREG_SET_ORDER, 0);

	if ( FALSE === strpos( $url, '.js' ) ) return $url;
	if ( $matches ) return $url;
	return "$url' defer ";
}
add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );

/* Tool functions */

// function to replace words inside a string
function str_lreplace($search, $replace, $subject) {
    $pos = strrpos($subject, $search);

    if($pos !== false)
    {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}


function left_sidebar_menu() {

// Print the first section of the menu
?>
	<nav class='side_bar-menu'>

		<a class='logo' href='/'>Johnny Monsarrat</a>

		<p class='logo_title'><?php bloginfo( 'name' ); ?></p>

		<a class='nav_item' href='/'>

			<span>home</span>

		</a>

		<div class='nav_item'>

			<span>story</span>

<?php

	$arg = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'meta_key' => '_wp_page_template',
		'meta_value' => 'page-chapter.php'
	);

	$chapters = get_pages( $arg );

	foreach ( $chapters as $chapter ) { 

		$page_id = $chapter->ID;

		$page_title = get_the_title( $page_id );

		$page_url = get_permalink( $page_id );

		$page_thumb = get_the_post_thumbnail_url( $page_id, "menu-micro" );

		$page_slug = $chapter->post_name;

		$chapter_number = str_replace('-',' ',$page_slug);

?>
			<a class='nav-story_item' href='<?php echo $page_url; ?>'>

				<div class='point'></div>

				<div class='nav-story_item-wrap'>

					<div class='chapter_img'>

						<img src='<?php echo $page_thumb; ?>'>

					</div>

					<div class='chapter_title'>

						<p><?php echo $chapter_number; ?></p>

						<h2><?php echo $page_title; ?></h2>

					</div>

				</div>

			</a>

		<?php

	} ?>

		</div><!-- END of 'nav_item' -->

		<a class='nav_item' href='/about-me'>

			<span>about me</span>

		</a>

	</nav>

<?php

	wp_reset_postdata();

}


function footer_menu() {

// Print the first section of the menu
?>
	<nav>

		<div class='progress'></div>

<?php

	footer_menu_thumb_generator();	// Adds the home item

	$arg = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'meta_key' => '_wp_page_template',
		'meta_value' => 'page-chapter.php'
	);

	$chapters = get_pages( $arg );

	foreach ( $chapters as $chapter ) { 

		footer_menu_thumb_generator( $chapter );

	}

	echo '</nav>';

	wp_reset_postdata();

}


function footer_menu_thumb_generator( $chapter = false ) {

		if ( $chapter ) {

			$page_id = $chapter->ID;

			$page_slug = $chapter->post_name;

			$chapter_number = str_replace('-',' ',$page_slug);

		} else {

			$page_id = get_option('page_on_front');

			$chapter_number = 'home';

		}

		$page_title = get_the_title( $page_id );

		$page_url = get_permalink( $page_id );

		$page_thumb = get_the_post_thumbnail_url( $page_id, "menu-small" );

?>

			<a class='nav_item' href='<?php echo $page_url; ?>'>

				<div class='point'></div>

				<div class='nav_item-wrap'> <img src='<?php echo $page_thumb; ?>'>

					<p><?php echo $chapter_number; ?></p>

					<h2><?php echo $page_title; ?></h2> </div>

			</a>

<?php

}