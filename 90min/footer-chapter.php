<?php
/**
 * The template for displaying the footer on chapters
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 90min
 */

?>


	<footer class='container-fluid'>

		<div class='container'>

			<?php footer_menu(); ?>

		</div>

	</footer>

<?php wp_footer(); ?>

</body>
</html>
