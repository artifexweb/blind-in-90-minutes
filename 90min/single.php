<?php
/**
 *
 * This is the template that displays single posts.
 * Please note that this is the WordPress construct of posts
 * and that other 'posts' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 90min
 */

get_header(); ?>

	<div class='wrap'>

	<?php
	if ( have_posts() ) :

		/* Start the Loop */
		while ( have_posts() ) : the_post();

			global $post;

			$metaVals = get_post_meta( get_the_ID() );

			foreach( $metaVals as $metakey => $metaval ) {

				if (substr ($metakey,0,1) != '_' ) {

					$key = str_replace('-','',$metakey);

					if ( count($metaval) > 1 ) {

						$$key=$metaval;

					}

					else {

						$$key = $metaval[0];

					}

				}

			} ?>

			<div class='container'>

				<div class='chapter_title'>

					<?php if ( !empty( $wpcftitle) ) : ?>

					<p class='sub_h1-1'><?php the_title(); ?></p>

					<h1 class='h1-1'><?php echo $wpcftitle; ?></h1>

					<?php else : ?>

					<h1 class='h1-1'><?php the_title(); ?></h1>

					<?php endif; ?>

			</div>

			<div class='content'>

				<?php if ( !empty( $wpcfchapterimages ) ) : ?>

				<div class='slider-wrap <?php echo $wpcfsingleormulti;?>'>

					<div class='slider'>

					<?php if ( is_array($wpcfchapterimages) ) : 

						foreach($wpcfchapterimages as $img): ?>

						<div class='slide'>

							<img src='<?php echo $img; ?>'>

						</div><!-- END 'slide' -->

						<?php endforeach;

					elseif ( $wpcfchapterimages ) : ?>

						<div class='slide'>

							<img src='<?php echo $wpcfchapterimages; ?>'>

						</div><!-- END 'slide' -->

					<?php endif; ?>

					</div><!-- END 'slider' -->

				</div><!-- END 'slider-wrap single' -->

				<?php elseif ( $thumb_image = get_the_post_thumbnail_url( $post->ID, 'blog-hero' ) ) : ?> 

				<div class='slider-wrap single'>

					<div class='slider'>

						<div class='slide'>

							<img src='<?php echo $thumb_image; ?>'>

						</div><!-- END 'slide' -->

					</div><!-- END 'slider' -->

				</div><!-- END 'slider-wrap single' -->

				<?php endif; ?>

				<div class='text'>

				<?php the_content(); ?>

				</div><!-- END 'text' -->

				<div class="nav_button">

				<?php if ( $prev = get_permalink(get_adjacent_post(false,'',false)) ) : ?>

					<a class="button button-outline_gray" href="<?php echo $prev;?>">Back</a>

				<?php endif; ?>

				<?php if ( $next = get_permalink(get_adjacent_post(false,'',true)) ) : ?>

				<a class="button button-outline_cyan" href="<?php echo $next;?>">Next</a>

				<?php endif; ?>

				</div><!-- END 'nav_button' -->

			</div><!-- END 'content' -->

		<?php endwhile;

	else :

		get_template_part( 'template-parts/content', 'none' );

	endif; ?>

	</div>

<?php
get_footer();
