<div class="col-md-4">
	<div class="blog_side_category">
		<div class="blog_side_category_nav">
			<ul class="clearfix">
				<li><a href="#tab_side_popular_blog" data-toggle="tab">Popular</a></li>
				<li class="active"><a  href="#tab_side_recent_blog" data-toggle="tab">Recent</a></li>
			</ul>
		</div>
		<div class="blog_side_category_list">
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane slide-left" id="tab_side_popular_blog">
					<ul class="clearfix">
						<?php
							$popularPostArgs = array( 
								'post_type' => 'post',
								'orderby' => 'date',
								'order' => 'DESC',
								'posts_per_page' => 4,
								'meta_key=post_views_count&orderby=meta_value_num&order=DESC'
							);
							$popular_posts = new WP_Query( $popularPostArgs );
							?>
							<!-- the loop -->
							<?php while ( $popular_posts->have_posts() ) : $popular_posts->the_post(); ?>
								<li>
									<a href="<?php get_permalink() ?>">
										<?php the_post_thumbnail() ; ?>
										<h3><?php the_title(); ?></h3>
										<p class="comment_count"><i class="fa fa-comments"></i> <?php comments_number( 'No Comment', '1 Comment', '% Comment'); ?></p>
									</a>
								</li>
							<?php endwhile; ?>
							<?php
								// Reset Query
								wp_reset_postdata();
							?>
							<?php echo do_shortcode('[ajax_load_more post_type="post" offset="4" posts_per_page="4" pause="true" scroll="false" transition="fade" button_label="More &raquo;" container_type=""]'); ?>
						<!--<a href="#" class="round_hover">More<i class="fa fa-caret-right"></i></a>-->
					</ul>
				</div>
				<div role="tabpanel" class="tab-pane slide-left in active" id="tab_side_recent_blog">
					<ul class="clearfix">
						<?php
							$recentPostArgs = array( 
								'post_type' => 'post',
								'orderby' => 'date',
								'order' => 'DESC',
								'posts_per_page' => 4,
							);
							$recent_posts = new WP_Query( $recentPostArgs );
							?>
							<!-- the loop -->
							<?php while ( $recent_posts->have_posts() ) : $recent_posts->the_post(); ?>
								<li>
									<a href="<?php get_permalink($recent_posts) ?>">
										<?php the_post_thumbnail() ; ?>
										<h3><?php the_title(); ?></h3>
										<p class="comment_count"><i class="fa fa-comments"></i> <?php comments_number( 'No Comment', '1 Comment', '% Comment'); ?></p>
									</a>
								</li>
							<?php endwhile; ?>
							<?php
								// Reset Query
								wp_reset_postdata();
							?>
							<?php echo do_shortcode('[ajax_load_more post_type="post" offset="4" posts_per_page="2" pause="true" scroll="false" transition="fade" button_label="More &raquo;" container_type=""]');?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="latest_news_wrap">
		<h3 class="latest_heading">Latest News</h3>
		<div class="latest_news_detail">
			<ul>
				<li>
					<a href="#">
						<h3>Microsoft Exposed</h3>
						<p>Time to find a new operating system as XP support ends Windows XP Support ends in</p>
						<h4><i class="fa fa-calendar"></i>19th October 2013</h4>
					</a>
				</li>
				<li>
					<a href="#">
						<h3>Microsoft Exposed</h3>
						<p>Time to find a new operating system as XP support ends Windows XP Support ends in</p>
						<h4><i class="fa fa-calendar"></i>19th October 2013</h4>
					</a>
				</li>
				<li>
					<a href="#">
						<h3>Microsoft Exposed</h3>
						<p>Time to find a new operating system as XP support ends Windows XP Support ends in</p>
						<h4><i class="fa fa-calendar"></i>19th October 2013</h4>
					</a>
				</li>
				<li class="load_more"><a href="#" class="round_hover">More<i class="fa fa-caret-right"></i></a></li>
			</ul>
		</div>
	</div>
</div>