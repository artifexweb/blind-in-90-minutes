<?php
/**
 * The template for displaying author pages
 * If you'd like to further customize these author views, you may create a
 * new template file for each one. For example, author-name.php will open page links with name
 */

	get_header(); 
	include_once("navHeader.php");
?>
<section class="page_title">
	<div class="container"><h2 class="text-left"><?php wp_title(''); ?></h2></div>
</section>

<section class="page_breadcumb_wrap">
	<div class="container">
		<div class="row">
			<div class="col-xs-6">
				<div class="page_name">Our Latest Spicy Blog Posts</div>
			</div>
			<div class="col-xs-6">
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
					<?php if(function_exists('bcn_display'))
					{
						bcn_display();
					}?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog_wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-8 author-page">
				<!-- This sets the $curauth variable -->
				<?php
				$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
				?>
				<h2>Author Name : <span><?php echo $curauth->nickname; ?></span></h2>
				<dl>
					<dt>Website</dt>
					<dd><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></dd>
					<dt>Profile</dt>
					<dd><?php echo $curauth->user_description; ?></dd>
				</dl>
			</div>
			
			<div class="col-md-4">
				<div class="author-sidebar blog_side_category">
					<div class="side_category_title">
						<h2>Posts by <?php echo $curauth->nickname; ?></h2>
					</div>
					<div class="blog_side_category_list">
						<ul>
						<!-- The Loop -->
						
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<li>
									<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
										<?php the_post_thumbnail() ; ?>
										<h3><?php the_title(); ?></h3>
										<p class="comment_count"><i class="fa fa-comments"></i> <?php comments_number( 'No Comment', '1 Comment', '% Comment'); ?></p>
									</a>
								</li>
						
							<?php endwhile; else: ?>
								<p><?php _e('No posts by this author.'); ?></p>
						
							<?php endif; ?>
						
							<!-- End Loop -->
						
							</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>

<div id="content" class="narrowcolumn">

<!-- This sets the $curauth variable -->

    <?php
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    ?>

    <h2>Author Name: <?php echo $curauth->nickname; ?></h2>
    <dl>
        <dt>Website</dt>
        <dd><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></dd>
        <dt>Profile</dt>
        <dd><?php echo $curauth->user_description; ?></dd>
    </dl>

    <h2>Posts by <?php echo $curauth->nickname; ?>:</h2>

    <ul>
<!-- The Loop -->

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <li>
            <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
            <?php the_title(); ?></a>,
            <?php the_time('d M Y'); ?> in <?php the_category('&');?>
        </li>

    <?php endwhile; else: ?>
        <p><?php _e('No posts by this author.'); ?></p>

    <?php endif; ?>

<!-- End Loop -->

    </ul>
</div>
<?php get_footer(); ?>