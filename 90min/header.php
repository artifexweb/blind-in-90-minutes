<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 90min
 */

 

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href='<?php echo THE_THEME ?>/images/favicon-16x16.png' rel='shortcut icon' type='image/vnd.microsoft.icon'>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0' name='viewport'>

<?php 

wp_head(); 

$body_class = 'nav_page';

if ( is_front_page() ) $body_class = 'index';

?>

</head>

<body <?php body_class( $body_class ); ?>>

	<?php left_sidebar_menu(); ?>

	<header>

		<button class='hamburger_menu' id='mobile_menu'> <span></span> </button> 

		<a href='/'><?php bloginfo( 'name' ); ?></a>

	</header>

	<!-- Start the content -->