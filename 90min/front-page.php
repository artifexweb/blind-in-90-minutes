<?php
/**
 * Template Name: Homepage
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 90min
 */

get_header(); ?>

	<div class='wrap hero'>

	<?php
	if ( have_posts() ) :

		/* Start the Loop */
		while ( have_posts() ) : the_post();

			global $post;

			$metaVals = get_post_meta( get_the_ID() );

			foreach( $metaVals as $metakey => $metaval ) {

				if (substr ($metakey,0,1) != '_' ) {

					$key = str_replace('-','',$metakey);

					if ( count($metaval) > 1 ) {

						$$key=$metaval;

					}

					else {

						$$key = $metaval[0];

					}

				}

			} ?>

			<div class='container'>

			<h1><?php bloginfo( 'name' ); ?></h1>

			<p class='sub_h1'><?php bloginfo( 'description' ); ?></p>

			<?php the_content(); ?>

			</div>


			<div class='jm'>

				<?php echo $wpcflowerrighttext; ?>

			</div> <img class='jm_photo' src='<?php echo get_the_post_thumbnail_url(); ?>'>

			<div class='bg_video'>

				<video autoplay='autoplay' loop='loop'>

					<source src='<?php echo $wpcfhomepagevideo; ?>' type='video/mp4'>

				</video>

			</div>

		<?php endwhile;

	else :

		get_template_part( 'template-parts/content', 'none' );

	endif; ?>

	</div>

<?php
get_footer('chapter');
