<?php
/**
 *
 * This is the template that displays single pages.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 90min
 */

get_header(); ?>

	<div class='wrap'>

	<?php
	if ( have_posts() ) :

		/* Start the Loop */
		while ( have_posts() ) : the_post();

			global $post;

			$metaVals = get_post_meta( get_the_ID() );

			foreach( $metaVals as $metakey => $metaval ) {

				if (substr ($metakey,0,1) != '_' ) {

					$key = str_replace('-','',$metakey);

					if ( count($metaval) > 1 ) {

						$$key=$metaval;

					}

					else {

						$$key = $metaval[0];

					}

				}

			} ?>

			<div class='container'>

				<div class='chapter_title'>

					<p class='sub_h1-1'><?php the_title(); ?></p>

					<h1 class='h1-1'><?php echo $wpcftitle; ?></h1>

			</div>

			<div class='content'>

				<?php if ( !empty($wpcfchapterimages) ) : ?>

				<div class='slider-wrap <?php echo $wpcfsingleormulti;?>'>

					<div class='slider'>

					<?php if ( is_array($wpcfchapterimages) ) : 

						foreach($wpcfchapterimages as $img): ?>

						<div class='slide'>

							<img src='<?php echo $img; ?>'>

						</div><!-- END 'slide' -->

						<?php endforeach;

					else : ?>

						<div class='slide'>

							<img src='<?php echo $wpcfchapterimages; ?>'>

						</div><!-- END 'slide' -->

					<?php endif; ?>

					</div><!-- END 'slider' -->

				</div><!-- END 'slider-wrap single' -->

				<?php endif; ?>

				<div class='text'>

				<?php the_content(); ?>

				</div><!-- END 'text' -->

				<div class="nav_button">

					<?php 
					$slug = $post->post_name;
					$slug_array = explode("-", $slug);
					$current_chapter = $slug_array[1];
					$prev_chapter = intval($current_chapter) - 1;
					$next_chapter = intval($current_chapter) + 1;
					?>

					<?php if ( intval($current_chapter) == 1 ) : ?>

					<a class="button button-outline_gray" href="/">Back</a>

					<?php else : ?>

					<a class="button button-outline_gray" href="/chapter-<?php echo $prev_chapter;?>">Back</a>

					<?php endif; ?>

					<?php if ( intval($current_chapter) < 10 ) : ?>

					<a class="button button-outline_cyan" href="/chapter-<?php echo $next_chapter;?>">Next</a>

					<?php endif; ?>

				</div><!-- END 'nav_button' -->

			</div><!-- END 'content' -->

		<?php endwhile;

	else :

		get_template_part( 'template-parts/content', 'none' );

	endif; ?>

	</div>

<?php
get_footer('chapter');
