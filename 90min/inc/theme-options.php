<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
    return false;
    
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'content'       => array( 
        array(
          'id'        => 'general_help',
          'title'     => 'General',
          'content'   => '<p>Help content goes here!</p>'
        )
      ),
      'sidebar'       => '<p>Sidebar content goes here!</p>'
    ),
    'sections'        => array( 
      array(
        'id'          => 'the_settings',
        'title'       => 'Settings'
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'theme-main-logo',
        'label'       => 'Update Logo for Header',
        'desc'        => 'Upload new logo for theme header.<br>Recommended Logo Size : 120x50px',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'the_settings'
      ),
      array(
        'id'          => 'theme-logo-text',
        'label'       => 'Logo text',
        'desc'        => 'Text to put under the logo',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'the_settings'
      ),
      array(
        'id'          => 'top_ribbon_contact_info',
        'label'       => 'Change the contact phone and email in header',
        'desc'        => 'Modify the contact info',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'the_settings',
        'settings'    => array( 
          array(
            'id'          => 'contact_info_header',
            'label'       => __( 'Contact Data', 'theme-contact-info' ),
            'desc'        => 'Paste your contact data',
            'type'        => 'text',
            'class'       => '' )
          )
      ),
      array(
        'id'          => 'homepage_banner',
        'label'       => 'Add Images to the Homepage Banner',
        'desc'        => 'Upload the corresponding image',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'the_settings',
        'settings'    => array( 
          array(
            'id'          => 'homepage_banner_item',
            'label'       => __( 'Slide Image', 'homepage-slider' ),
            'desc'        => 'Upload a new banner slide',
            'type'        => 'upload',
            'class'       => '' )
          )
      ),
      array(
        'id'          => 'footer_ribbon_contact_info',
        'label'       => 'Change the contact phone and email in footer',
        'desc'        => 'Modify the contact info',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'the_settings',
        'settings'    => array( 
          array(
            'id'          => 'contact_info_footer',
            'label'       => __( 'Contact Data', 'theme-contact-info' ),
            'desc'        => 'Paste your contact data',
            'type'        => 'text',
            'class'       => '' )
          )
      ),
      array(
        'id'          => 'theme-footer-logo',
        'label'       => 'Update Logo for Footer',
        'desc'        => 'Upload new logo for theme footer.<br>Recommended Logo Size : 200x100px',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'the_settings'
      ),
      array(
        'id'          => 'change_the_copyright_text',
        'label'       => 'Change the copyright text in footer',
        'desc'        => 'Modify the bottom text of this site',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'the_settings',
      ),
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}