<?php
	/**
	 * Custom image sizes added on the function by array merging
	 */
	if ( function_exists( 'add_image_size' ) ) {

		add_image_size( 'blog-hero', 360 ); // Blog post featured image

		add_image_size( 'menu-small', 180, 120, true );	//(cropped)

		add_image_size( 'menu-micro', 40, 40, true ); 	//(cropped)

	}

	function my_image_sizes($sizes) {

		$addsizes = array(

			"blog-hero" => __( "Blog Hero", '90min'),

			"menu-small" => __( "Menu Small", '90min'),

			"menu-micro" => __( "Menu Micro", '90min')

		);

		$newsizes = array_merge($sizes, $addsizes);

		return $newsizes;

	}

	add_filter('image_size_names_choose', 'my_image_sizes');

?>