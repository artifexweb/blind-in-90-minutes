<?php

/**
 * Required: include Facet Search for Wordpress.
 * Description: Advanced Filtering for WordPress
 */
require get_template_directory() . '/plugins/facetwp/index.php'; 



/**
 * Required: include no captcha recaptcha.
 * Description: Secures wordpress login to prevent bruteforce attacks
 */
require get_template_directory() . '/plugins/no-captcha-recaptcha/no-captcha-recaptcha.php'; 



/**
* Dee.ie Theme uses Option Tree Plugin in many area
* Theme Options Panel area
* Meta Box custom field in custom post area and more. 
* So be careful when removing option tree plugin from this theme
*/


/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );


/**
 * Required: include OptionTree.
 */
require( trailingslashit( get_template_directory() ) . '/plugins/option-tree/ot-loader.php' );


/**
 * Required: OptionTree Theme Option
 */
require( trailingslashit( get_template_directory() ) . '/inc/theme-options.php' );

