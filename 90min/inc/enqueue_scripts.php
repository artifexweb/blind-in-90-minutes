<?php

function the_scripts() {

	/**
	* adding js scripts for the theme
	*/

	// Externalize jquery
	if (!is_admin()) {
		wp_deregister_script('jquery');
	//	wp_register_script('jquery', 'https://code.jquery.com/jquery-1.12.4.min.js', false, '1.12.4');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-3.1.1.min.js', false, '3.1.1');
		wp_enqueue_script('jquery');
	}

	// Add Slick JS
	wp_enqueue_script( 'owl_carousel_js', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array('jquery'), '1.6.0', 'true' );


	// Add Custom Dee Script
	wp_enqueue_script( 'the-script', get_template_directory_uri() . '/js/script.js', array('jquery'), THE_VERSION, 'true' );


	/**
	* adding additional styles for the theme
	*/

	// Add font-awesome
	wp_enqueue_style( 'fontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '4.7.0', 'all' );

	// Add theme fonts Css
	wp_enqueue_style('theme-fonts', get_template_directory_uri() . '/css/fonts.css', array(), '1.0', 'all');

	// Add vendor Css
	wp_enqueue_style( 'vendor-css', get_template_directory_uri() . '/css/style_vendor.min.css', array(), '1.0', 'all' );

	// Add custom Css
	wp_enqueue_style( 'the-style', get_template_directory_uri() . '/style.css', array(), THE_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'the_scripts' );
