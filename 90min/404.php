<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package 90min
 */

get_header(); ?>

	<div class='wrap'>

		<div class='container'>

			<div class='chapter_title'>

				<p class='sub_h1-1'>404 - Not Found</p>

				<h1 class='h1-1'>Oops! That page can&rsquo;t be found.</h1>

			</div>

			<div class='content'>

				<div class='text'>

				<p>It looks like nothing was found at this location. Maybe try one of the links on the menu; </p>

				</div><!-- END 'text' -->

			</div><!-- END 'content' -->

		</div><!-- END 'container' -->

	</div><!-- END 'wrap' -->

<?php
get_footer();
